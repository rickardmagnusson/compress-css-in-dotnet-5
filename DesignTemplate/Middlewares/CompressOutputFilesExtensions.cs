﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace DesignTemplate
{
    public static class CompressOutputFilesExtensions
    {
        /// <summary>
        /// Enables compressed css files to output response.
        /// </summary>
        /// <param name="app"></param>
        public static void UseCompressedCss(this IApplicationBuilder app)
        {
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = context =>
                {
                    IHeaderDictionary headers = context.Context.Response.Headers;
                    string contentType = headers["Content-Type"];

                    if (contentType == "application/x-gzip")
                    {
                        if (context.File.Name.EndsWith("css.gz")) {
                            contentType = "text/css";
                        }
                        headers.Add("Content-Encoding", "gzip");
                        headers["Content-Type"] = contentType;
                    }
                    else { 
                        headers["Content-Type"] = contentType;
                    }
                }
            });
        }
    }
}
