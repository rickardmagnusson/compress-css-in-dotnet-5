﻿using Microsoft.AspNetCore.Components;
using System.Collections.Generic;

namespace DesignTemplate.Pages
{
    public partial class Index : ComponentBase
    {
        public class Item
        {
            public string Name { get; set; }
            public string LName { get; set; }
        }

        private List<Item> Data { get; set; } = new List<Item>
        {
            new Item{ Name= "Rickard", LName="Magnusson" },
            new Item{ Name= "Rickard", LName="Magnusson" },
            new Item{ Name= "Rickard", LName="Magnusson" },
            new Item{ Name= "Rickard", LName="Magnusson" },
            new Item{ Name= "Rickard", LName="Magnusson" },
            new Item{ Name= "Rickard", LName="Magnusson" },
            new Item{ Name= "Rickard", LName="Magnusson" },
            new Item{ Name= "Rickard", LName="Magnusson" }
        };
    }
}
