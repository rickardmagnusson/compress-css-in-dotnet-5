# Compress Sass files to .gz in ,NET5 #

Startup.cs
Configure services -->

    services.AddResponseCompression(options =>
    {
        options.EnableForHttps = true;
        options.Providers.Add<GzipCompressionProvider>();
    });

Configure -->
    
    Used by.gz must be before UseStaticFile()
    app.UseCompressedCss();
    app.UseStaticFiles();